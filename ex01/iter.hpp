/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/19 13:52:55 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/19 14:27:11 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ITER_HPP
#define ITER_HPP

#include <cstddef>

/*
 * This function applies to each element of array (given its length) the passed
 * function.
 */
template <typename T>
void
iter(T* array, size_t lenght, void (*func)(T& thing))
{
	for (size_t i = 0; i < lenght; i++)
		func(array[i]);
}

#endif
