/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/18 20:30:41 by elopez-r          #+#    #+#             */
/*   Updated: 2021/07/18 20:50:38 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WHATEVER_HPP
#define WHATEVER_HPP

template <typename T>
void
swap(T& a, T& b)
{
	T aux;

	aux = a;
	a = b;
	b = aux;
}

template <typename T>
T&
min(T& a, T& b)
{
	return (a < b ? a : b);
}

template <typename T>
T&
max(T& a, T& b)
{
	return (a > b ? a : b);
}

#endif
